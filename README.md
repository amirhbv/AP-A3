# AP Assignment 3

## Huffman Coding

### Description
c++ Huffman coding program,
it can compress files by huffman coding and decompress them,
it also can encode them by [Caesar Cipher](https://learncryptography.com/classical-encryption/caesar-cipher),
it uses SDBM hashing method to check files

[Huffman coding](https://www.siggraph.org/education/materials/HyperGraph/video/mpeg/mpegfaq/huffman_tutorial.html) is a way of compressing data,
it makes a tree from given data from input file and generate a code for every character, the more occurrence the character have the shorter it's code become,
    then make a new binary string using those codes and store all of them in output file.

### usage
```sh
g++ main.cpp
a.out encode <inputFileName> <outputFileName> <kNumber>
a.out decode <inputFileName> <outputFileName> <kNumber>
a.out compress_only <inputFileName> <outputFileName>
a.out decompress_only <inputFileName> <outputFileName>
```
knumber will be used for Caesar

