#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <vector>
#include <queue>
#include <ctime>
using namespace std;
#define EIGHT 8
#define MAXN 256

struct HuffmanTreeNode{
    unsigned char data;
    int frequency;
    string huffmanCode;
    HuffmanTreeNode *right, *left;

    HuffmanTreeNode(){}
    HuffmanTreeNode(char data, int frequency)
    {
        this->data = data;
        this->frequency = frequency;
        this->huffmanCode = "";
        this->left = this->right = NULL;
    }
};

string readFromFile(string);
void writeToFile(long long int, string, string);
vector<HuffmanTreeNode> charRepeatCounter(string);
HuffmanTreeNode buildHuffmanTree(vector<HuffmanTreeNode>);
void setHuffmanCodes(HuffmanTreeNode*, vector<HuffmanTreeNode>&, string);
string huffmanEncode(vector<HuffmanTreeNode>, string);
void printHuffmanCodes(vector<HuffmanTreeNode>);
string makeHuffmanFileContent(vector<HuffmanTreeNode>, string);
int binaryToDecimal(string);
string decimalToBinary(int);
string binaryStringToCharacter(string);
string decimalToString(int);
int stringToDecimal(string);

string caesarEncode(string, int);
string caesarDecode(string, int);

long long int SDBM(string);

bool checkHash(string);
string readFromEncodedFile(string);
string huffmanDecode(string);
string characterToBinaryString(string);
string decodeByHuffmanCodes(string, vector<string>);
void writeToFile(string, string);

void test();
void testBinaryToDecimal();
void testDecimalToBinary();
void testCaesarEncode();
void testCaesarDecode();
void testDecimalToString();
void testStringToDecimal();
void testSDBM();
void testCheckHash();
void testCharacterToBinaryString();
void testBinaryStringToCharacter();
void makeBigRandomFile();

#define NDEBUG
int main(int argc, char** argv)
{
    test();

    string command = argv[1];
    string inputFileName = argv[2];
    string outputFileName = argv[3];
    if(command == "encode" || command == "compress_only")
    {
        string fileContents = readFromFile(inputFileName);
        vector<HuffmanTreeNode> charFrequency = charRepeatCounter(fileContents);
        HuffmanTreeNode root = buildHuffmanTree(charFrequency);
        setHuffmanCodes(&root, charFrequency, "");
        //printHuffmanCodes(charFrequency);
        string encodedFileContents = huffmanEncode(charFrequency, fileContents);
        string newFileContents = makeHuffmanFileContent(charFrequency, encodedFileContents);

        if(command == "encode")
        {
            string key = argv[4];
            newFileContents = caesarEncode(newFileContents, stringToDecimal(key));
        }

        writeToFile(SDBM(newFileContents), newFileContents, outputFileName);
    }
    else if(command == "decode" || command == "decompress_only")
    {
        if(!checkHash(inputFileName))
        {
            cout << "The File is Changed !\n";
            return 0;
        }
        string fileContents = readFromEncodedFile(inputFileName);
        if(command == "decode")
        {
            string key = argv[4];
            fileContents = caesarDecode(fileContents, stringToDecimal(key));
        }
        string decodedFileContents =  huffmanDecode(fileContents);
        writeToFile(decodedFileContents, outputFileName);
    }
    return 0;
}

string readFromFile(string inputFileName)
{
    ifstream fin;
    fin.open(inputFileName.c_str());
    string result;
    char temp;
    while(fin.get(temp))
        result += temp;
    fin.close();
    return result;
}

void writeToFile(long long int hash, string fileContents, string outputFileName)
{
    ofstream fout;
    fout.open(outputFileName.c_str());
    fout << hash << endl;
    fout << fileContents;
    fout.close();
}

vector<HuffmanTreeNode> charRepeatCounter(string fileContents)
{
    vector<HuffmanTreeNode> charFrequency;
    for(int i = 0 ; i < 256 ; i++)
        charFrequency.push_back(HuffmanTreeNode(char(i), 0));
    for(unsigned int i = 0 ; i < fileContents.size() ; i++)
        charFrequency[fileContents[i]].frequency++;
    return charFrequency;
}

bool operator<(const HuffmanTreeNode& A, const HuffmanTreeNode& B)
{
    return A.frequency > B.frequency;
}

HuffmanTreeNode buildHuffmanTree(vector<HuffmanTreeNode> charFrequency)
{
    priority_queue<HuffmanTreeNode> charFrequencyQueue;
    for(unsigned int i = 0; i < charFrequency.size(); i++)
        charFrequencyQueue.push(charFrequency[i]);
    while(charFrequencyQueue.size() > 1)
    {
        HuffmanTreeNode *top = new HuffmanTreeNode;
        top->left = new HuffmanTreeNode;
        *(top->left) = charFrequencyQueue.top();
        charFrequencyQueue.pop();
        top->right = new HuffmanTreeNode;
        *(top->right) = charFrequencyQueue.top();
        charFrequencyQueue.pop();
        top->frequency = top->right->frequency + top->left->frequency;
        charFrequencyQueue.push(*top);
    }
    return charFrequencyQueue.top();
}

void setHuffmanCodes(HuffmanTreeNode* root, vector<HuffmanTreeNode>& charFrequency, string code)
{
    if(!root)
        return;
    if(root->left == NULL && root->right == NULL)
    {
        charFrequency[(root->data)].huffmanCode = code;
        return;
    }
    setHuffmanCodes(root->left, charFrequency, code + '0');
    setHuffmanCodes(root->right, charFrequency, code + '1');
}

string huffmanEncode(vector<HuffmanTreeNode> charFrequency, string fileContents)
{
    string result;
    for(unsigned int i = 0; i < fileContents.size(); i++)
        result += charFrequency[fileContents[i]].huffmanCode;
    return result;
}

void printHuffmanCodes(vector<HuffmanTreeNode> charFrequency)
{
    for(unsigned int i = 0; i < charFrequency.size(); i++)
        cout << charFrequency[i].data << ' ' << charFrequency[i].huffmanCode << endl;
}

string makeHuffmanFileContent(vector<HuffmanTreeNode> charFrequency, string encodedFileContents)
{
    string result;
    for(unsigned int i = 0; i < charFrequency.size(); i++)
        result += (charFrequency[i].huffmanCode + ' ');
    result += ( decimalToString(encodedFileContents.size() )+ '\n' );
    while(encodedFileContents.size() % EIGHT)
        encodedFileContents += '0';
    result += binaryStringToCharacter(encodedFileContents);
    return result;
}

string binaryStringToCharacter(string encodedFileContents)
{
    string result;
    for(unsigned int i = 0; i < encodedFileContents.size(); i += 8)
        result += (char)( binaryToDecimal(encodedFileContents.substr(i, 8)) );
    return result;

}

int binaryToDecimal(string binaryNumber)
{
    reverse(binaryNumber.begin(), binaryNumber.end());
    int result = 0;
    for(unsigned int i = 0; i< binaryNumber.size(); i++)
        result += ((binaryNumber[i] - '0') * (1<<i) );
    return result;
}

long long int SDBM(string content)
{
    long long int hash = 0 ;
    for(unsigned int i = 0; i < content.size(); i++)
        hash = (hash<<6) + (hash<<16) - hash + content[i];
    return hash;
}

string caesarEncode(string fileContents, int key)
{
    for(unsigned int i = 0; i < fileContents.size(); i++)
        fileContents[i] += key;
    return fileContents;
}
string caesarDecode(string encoded, int key)
{
    for(unsigned int i = 0; i < encoded.size(); i++)
        encoded[i] -= key;
    return encoded;
}


string decimalToString(int decimalNumber)
{
    string result;
    while(decimalNumber)
    {
        result += ((decimalNumber % 10) + '0' );
        decimalNumber /= 10;
    }
    reverse(result.begin(), result.end() );
    return result;
}

int stringToDecimal(string decimalNumber)
{
    int result = 0;
    for(unsigned int i = 0; i < decimalNumber.size(); i++)
    {
        result *= 10;
        result +=  (decimalNumber[i] - '0' );
    }
    return result;
}

bool checkHash(string inputFileName)
{
    ifstream fin;
    fin.open(inputFileName.c_str());
    long long int oldHash;
    fin >> oldHash;
    string fileContents;
    char temp;
    fin.get(temp); // '\n'
    while(fin.get(temp))
        fileContents += temp;
    long long int newHash = SDBM(fileContents);
    return (oldHash == newHash);
}

string readFromEncodedFile(string inputFileName)
{
    string result;
    long long hash;
    ifstream fin;
    fin.open(inputFileName.c_str());
    fin >> hash;
    char temp;
    fin.get(temp); // '\n'
    while(fin.get(temp))
        result += temp;
    return result;
}

string huffmanDecode(string fileContents)
{
    stringstream stream(fileContents);
    vector<string> huffmanCodes;
    for(int i = 0; i < 256; i++)
    {
        string temp;
        stream >> temp;
        huffmanCodes.push_back(temp);
    }
    int numberOfbits;
    stream >> numberOfbits;
    string encodedString;
    char temp;
    stream.get(temp); // \n
    while(stream.get(temp))
        encodedString += temp;
    string binaryString = characterToBinaryString(encodedString);
    binaryString.resize(numberOfbits);

    return decodeByHuffmanCodes(binaryString, huffmanCodes);
}

string decodeByHuffmanCodes(string binaryString, vector<string> huffmanCodes)
{
    string buffer, result;
    for(unsigned int i = 0; i < binaryString.size(); i++)
    {
        buffer += binaryString[i];
        vector<string>::iterator it = find(huffmanCodes.begin(), huffmanCodes.end(), buffer);
        if(it != huffmanCodes.end())
        {
            int index = it - huffmanCodes.begin();
            result += (unsigned char)(index);
            buffer = "";
        }
    }
    return result;
}

void writeToFile(string fileContents, string outputFileName)
{
    ofstream fout;
    fout.open(outputFileName.c_str(), ofstream::out | ofstream::trunc);
    fout << fileContents;
    fout.close();
}

string characterToBinaryString(string encodedString)
{
    string result;
    for(unsigned int i = 0; i < encodedString.size(); i++)
    {
        result += decimalToBinary((int)((unsigned char)encodedString[i]));
    }
    return result;
}

string decimalToBinary(int decimalNumber)
{
    string result;
    while(decimalNumber)
    {
        result += (abs(decimalNumber%2) + '0');
        decimalNumber /= 2;
    }
    while(result.size() < 8)
        result += '0';
    reverse(result.begin(), result.end());
    return result;
}


void testBinaryToDecimal()
{
    assert(binaryToDecimal("11111111") == 255);
    assert(binaryToDecimal("00000000") == 0);
    assert(binaryToDecimal("00000001") == 1);
    assert(binaryToDecimal("00000100") == 4);
}
void testCaesarEncode()
{
    assert(caesarEncode("aa", 2) == "cc");
    assert(caesarEncode("cc", -2) == "aa");
}

void testDecimalToString()
{
    assert(decimalToString(153) == "153");
    assert(decimalToString(14040) == "14040");
}
void testStringToDecimal()
{
    assert(stringToDecimal("178") == 178);
    assert(stringToDecimal("1040") == 1040);
}
void testCaesarDecode()
{
    assert(caesarDecode("ddd", 3) == "aaa");
    assert(caesarDecode("aaa", -3) == "ddd");
}
void testSDBM()
{
    assert(SDBM("a") == 97);
    assert(SDBM("amir") == 27382357152156821);
}
void testCheckHash()
{
    string s = "aaabb";
    ofstream fout("temp.txt");
    fout << SDBM(s) << endl << s;
    fout.close();
    assert(checkHash("temp.txt") );
}

void testCharacterToBinaryString()
{
    assert(characterToBinaryString("a") == "01100001");
}

void testBinaryStringToCharacter()
{
    assert(binaryStringToCharacter("01100010") == "b");
}

void testDecimalToBinary()
{
    assert(decimalToBinary(10) == "00001010");
}

void makeBigRandomFile()
{
    ctime(NULL);
    ofstream fout;
    fout.open("in.txt");
    for(int i = 0 ; i < 3e7 ; i++)
        fout << (char)((rand() % 128));
    fout.close();
}

void test()
{

    testBinaryToDecimal();
    testCaesarEncode();
    testCaesarDecode();
    testDecimalToString();
    testStringToDecimal();
    testSDBM();
    //testCheckHash();
    testCharacterToBinaryString();
    testDecimalToBinary();
    testBinaryStringToCharacter();
}
